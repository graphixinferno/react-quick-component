#! /usr/bin/env node

const { program } = require('commander');
const create = require('./commands/create');

program
    .command('create')
    .description('creat all the components')
    .option('--dir', 'Sets path for saving file.')
    .option('-t', 'Detects if the test flag is present.')
    .option('-m', 'Detects if the mock flag is present.')
    .option('-s', 'Detects if the style flag is present.')
    .option('-f', 'Detects if the fixture flag is present.')
    .action(create)
    .parse(process.argv)


