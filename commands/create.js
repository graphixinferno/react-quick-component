const fs = require('fs');
const { render } = require('template-file');

require.extensions['.txt'] = function (module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
};

const filename = process.argv[3];
const folder = filename.charAt(0).toLowerCase() + filename.slice(1);

const directoryIndex = process.argv.indexOf('--dir');
const directory = directoryIndex > -1 ? process.argv[directoryIndex + 1] :'./';

const test = process.argv.indexOf('-t') > -1;
const mock = process.argv.indexOf('-m') > -1;
const fixture = process.argv.indexOf('-f') > -1;
const style = process.argv.indexOf('-s') > -1;

const componentString = require("../templates/component.txt");
const fixtureString = require("../templates/fixture.txt");
const mockString = require("../templates/mock.txt");
const testString = require("../templates/test.txt");

function create () {

    /** check directory **/

    if (!fs.existsSync(directory)){
        fs.mkdirSync(directory,{recursive: true})
    }

    /** create folder **/

    if (!fs.existsSync(`${directory}/${folder}`)){
        fs.mkdirSync(`${directory}/${folder}`,{recursive: true})
    }

    /** create Component **/

    let renderComponentString = render(componentString, {filename: filename});

    fs.writeFile(`${directory}/${folder}/${filename}.js`, renderComponentString, err => {
        if (err) return console.error(`Yikes! Failed to create file: ${err.message}.`);
        console.log('Saved template!');
    });

    /** create scss file **/

    if(style){

        let styleFile = `${directory}/${folder}/${filename}.module.scss`;

        fs.writeFile(styleFile, '', err => {
            if (err) return console.error(`Yikes! Failed to create file: ${err.message}.`);
            console.log(`Stylesheet created`);
        });

    }

    /** create test **/

    if(test){

        let renderTestString = render(testString, {filename: filename});

        fs.writeFile(`${directory}/${folder}/${filename}.test.js`, renderTestString, err => {
            if (err) return console.error(`Yikes! Failed to create file: ${err.message}.`);
            console.log('Saved template!');
        });

    }

    /** create fixture **/

    if(fixture){

        let renderFixtureString = render(fixtureString, {filename: filename});

        fs.writeFile(`${directory}/${folder}/${filename}.fixture.js`, renderFixtureString, err => {
            if (err) return console.error(`Yikes! Failed to create file: ${err.message}.`);
            console.log('Saved template!');
        });

    }

    /** create fixture **/

    if(mock){

        let renderMockString = render(mockString, {filename: filename});

        fs.writeFile(`${directory}/${folder}/${filename}.mock.js`, renderMockString, err => {
            if (err) return console.error(`Yikes! Failed to create file: ${err.message}.`);
            console.log('Saved template!');
        });

    }

}

module.exports = create